FROM centos:centos7
MAINTAINER Nikhil <nikhil.tivo@gmail.com>

RUN yum update -y; yum clean all
RUN yum -y install openssh-server openssh-clients chpasswd net-tools less; yum clean all
RUN mkdir /var/run/sshd
RUN echo 'root:changeme' | chpasswd
RUN sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN ssh-keygen -P "" -t rsa -f /etc/ssh/ssh_host_rsa_key

#Update pam.d for proper login
RUN sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

EXPOSE 22
ENTRYPOINT ["/usr/sbin/sshd", "-D"]
