#!/bin/bash
#Created by Nikhil Narayanan on August 5th
#Script to get the hostname and IP address from running docker container

FILE=names
if [ -f $FILE ]; then
	rm -rf $FILE 	
	docker ps | awk '{if(NR>1)print $1}' > names
else
	docker ps | awk '{if(NR>1)print $1}' > names
fi
#Read values and compute
while read -r line
do	
	docker inspect $line --format='{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}  {{ .Config.Hostname }}'
done < $FILE
#Remove files
rm -rf $FILE

